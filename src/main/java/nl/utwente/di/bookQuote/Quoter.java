package nl.utwente.di.bookQuote;

import java.awt.desktop.QuitEvent;
import java.util.HashMap;

/**
 * Example of a Servlet that gets an ISBN number and returns the book price
 */

public class Quoter {

    HashMap<String, Double> bookList;

    public Quoter(){
        bookList = new HashMap<>();
        bookList.put("1", 10.0);
        bookList.put("2", 45.0);
        bookList.put("3", 20.0);
        bookList.put("4", 35.0);
        bookList.put("5", 50.0);
    }

    public double getBookPrice(String book){
        if (bookList.containsKey(book)) {
            return bookList.get(book);
        }
        return 0.0;
    }
}
